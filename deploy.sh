#!/usr/bin/env bash

if [ -z "$1" ]
then
    echo "You have to pass the environment: dev, homolog, live"
    echo
    exit 1
fi

ENV="$1"

if [ "$ENV" == "dev" ]; then
    usdocker docker-compose up local dev "worker-image"

elif [ "$ENV" == "live" ]; then
    echo "Deploying LIVE"
    usdocker docker-compose up worker live "worker-image"
fi
