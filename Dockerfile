FROM byjg/php7:alpine

VOLUME /opt/worker
WORKDIR /opt/worker

RUN COMPOSER_HOME="/opt/.composer" composer global require "byjg/php-daemonize=dev-master" \
 && ln -s /opt/.composer/vendor/bin/daemonize /usr/local/bin/

COPY src /opt/worker
COPY vendor /opt/worker

# Setup DateFile
RUN apk add --no-cache --update tzdata
ENV TZ=America/Sao_Paulo

RUN cd /opt/worker \
 && daemonize install tryme \
    --template=initd \
    "\\BeansqueueWorker\Process\Daemon::execute" \
    "vendor/autoload.php" \
    "./" \
    "" \
    'queueName=BeansqueueWorker.Sample.TryMe' \
    'queueConfig={"serverIp":"beanstalkd", "maxThreads": "4", "autoloadPath":"vendor/autoload.php", "workingPath":"./"}'

#RUN composer update

