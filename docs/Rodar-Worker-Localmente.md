# Rodar o Worker localmente

## Instalar o Daemonize (somente na primeira vez)

```
composer global require "byjg/php-daemonize=1.2.*"
sudo ln -s ~/.composer/vendor/bin/daemonize /usr/local/bin
```

## Configurar a sua fila no banco de dados

- Inserir na tabela 'queue_worker' (no servidor beanstalkd) o namespace completo. 
- Guardar o ID

## Rodar localmente (sem buscar payload no servidor)

```
cd /FOLDER/SEU_PROJETO
daemonize run "\\BeansqueueWorker\Process\CronTab::execute" "vendor/autoload.php" \
    "./" \
    'queue=ID' \
    'payload={}'
```

Lembrar que :

- ID - Deve ser o ID dentro da tabela 'queue_worker'
- O payload deve ser o JSON que vc passar para o projeto. 


### Instalar BeansqueueWorker

Entre na pasta onde o seu serviço está instalado corretamente e execute o comando abaixo:

```
sudo daemonize install SEU-SERVICO "\\BeansqueueWorker\Process\Daemon::execute" "vendor/autoload.php" \
    "./" "" 
    queue=ID
```

Lembrar que :

- ID - Deve ser o ID dentro da tabela 'queue_worker'
- SEU-SERVICO - O nome que será utilizado em "service SEU-SERVICO start"


### Postar uma mensagem no Beanstalk
```
curl -X POST "http://104.236.25.189/1.0/queue.json" \
    -d '{
            "queue": "BeansqueueWorker.Sample.TryMe", 
            "payload": { "a": "b6" }, 
            "timeout": 60
        }
    '
```


### Verificar o log de erros
```
tail -n 200 -f /var/log/daemonize/SEU-SERVICO.error.log 
```

### Verificar o log de execução
```
tail -n 200 -f /var/log/daemonize/SEU-SERVICO.log 
```

### Desinstalar 
```
sudo daemonize uninstall SEU-SERVICO
```


