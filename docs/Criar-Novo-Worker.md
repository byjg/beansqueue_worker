# Criando um novo Worker

## Criar um projeto 

- Crie um projeto no Netbeans
- Colocar no mínimo o seguinte Composer.json

```json
    "require": {
        "byjg/beansqueueworker": "dev-master",
    },
        "minimum-stability": "dev"
    }
```

## Criar o Worker

```php

// Esse Namespace será também o NOME da fila no BeansqueueWorker
namespace BeansqueueWorker\Sample;

use BeansqueueWorker\Worker\BaseWorker;

class TryMe extends BaseWorker
{
    public function execute()
    {

        // Aqui está a mensagem que deve ser tratada. 
        $data = $this->getPayload();

        // SEMPRE deve retornar ou TRUE ou FALSE. 
        // Se retornar 'true' a execução foi bem sucedida e vai para Done. 
        // Se retornar 'false' ou uma exceção a execução foi mal sucedida e vai para "bury"
        return true;
    }
}
```

