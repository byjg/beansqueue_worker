# Beansqueue Worker

O sistema BeansqueueWorker permite criar uma Fila e um Worker que irá consumir a fila.

Para isso é necessário os seguintes elementos:
* Fila (implementada em Beanstalk)
* Enfileiramento (implementada no BeansqueueWorker)
* Consumo (Worker, implementada BeansqueueWorker e php-daemonize)

## Funcionamento

O funcionamento básico de uma fila é o seguinte:
* Um produtor 
* A Fila
* O Consumidor

![Queue](queue.png)

O BeansqueueWorker Worker possibilita intermediar a produção, acesso à fila e o consumo das mensagens. Dessa forma temos os seguintes elementos:
* Um produtor --> Post de uma mensagem usando o CURL (ver abaixo)
* A Fila --> Beanstalkd
* O Consumidor --> Adicionar o BeansqueueWorker como dependencia no composer e criar um projeto que herda de "BaseWorker"

![Componentes](components.png)

Veja na pasta DOCS alguns tutorias.

# Instalando o Beanstalkd (Gerenciador da Fila)

A fila está implementada usando o Beanstalk. A instalação básica é:

```
sudo apt-get install beanstalkd
```

E a configuração em /etc/default/beanstalkd

```
BEANSTALKD_LISTEN_ADDR=127.0.0.1
BEANSTALKD_LISTEN_PORT=11300
BEANSTALKD_EXTRA="-b /var/lib/beanstalkd"
```

Altere o BEANSTALKD_LISTEN_ADDR para o IP da máquina


# Criação do Consumer (Worker)

O Enfileiramento requer que você passa um JSON contendo o nome do Worker e o conteúdo do 
payload que deverá ser passado como parâmetro.

## Adicione ao seu projeto

```
composer require byjg/beansqueueworker
```

## Crie uma classe que implementa \BeansqueueWorker\Worker\BaseWorker

```php
<?php
namespace Caminho\Da\Classe;

class MinhaClasse extends \BeansqueueWorker\Worker\BaseWorker
{

    /**
     * Toda a implementação do Job. O Payload está em DATA. Normalmente é um String.
     */
    public function execute()
    {
        // Get Payload
        $data = $this->getPayload();
        
        // Execute
        // ...
        
        // Very important retry true
        return true;
    }
}
```

Importante observar que o Namespace completo da classe será utilizado para o enfileiramento.
No exemplo `Caminho.Da.Classe.MinhaClasse'.

É importante também que a sua classe esteja também no composer com o vendor/autoload.

## Instalar o serviço de WORKER no servidor

Requer o PHP-Daemonize.

Para instalar o PHP Daemonize siga os passos (rode como root! - `sudo su -`)

```
composer global require "byjg/php-daemonize=1.3.*"
sudo ln -s /home/ubuntu/.composer/vendor/bin/daemonize /usr/local/bin
```

Para ficar disponível para outros usuários também execute a seguinte linha:

```
chmod a+x /root
```

Após feito isso devemos instalar um serviço para cada WORKER. Por exemplo, imagine o Worker chamado Mailer.

Para instalar use o seguinte comando  (como root):

```
cd /Diretorio/Projeto
daemonize install name-of-my-job \
    --template=initd \
    "\\BeansqueueWorker\Process\Daemon::execute" \
    "vendor/autoload.php" \
    "./" \
    "" \
    'queueName=Caminho.Da.Classe.MinhaClasse' \
    'queueConfig={"serverIp":"beanstalkd", "maxThreads": "1", "autoloadPath":"vendor/autoload.php", "workingPath":"./"}' 
```

Uma vez configurado, para iniciar basta rodar:

```
service name-of-my-job start
```

# Testando o seu Worker

## Rodar o Worker standalone passando o Payload  

Utilize essa opção para testar o seu Worker

```
daemonize run "\\BeansqueueWorker\\Process\\CronTab::execute" \
    "vendor/autoload.php" \
    "./" \
    'queueName=BeansqueueWorker.Sample.TryMe' \
    'payload={"a": "10"}'
```

## Rodar Daemon consumindo o Payload do servidor beansktalk

```
daemonize run "\\BeansqueueWorker\\Process\\Daemon::execute" \
    "vendor/autoload.php" \
    "./" \
    'queueName=BeansqueueWorker.Sample.TryMe' \
    'queueConfig={"serverIp":"beanstalkd", "maxThreads": "1"}' 
```

## Rodar o Worker em testes unitários

A classe `execute` requerida pela implementação da interface contém todo o código necessário para
que a sua aplicação possa rodar. 

Sendo assim, basta instancia-la e aplicar os testes unitários necessários. 