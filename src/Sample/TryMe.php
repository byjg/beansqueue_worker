<?php

namespace BeansqueueWorker\Sample;

use BeansqueueWorker\Worker\BaseWorker;

class TryMe extends BaseWorker
{
    public function execute()
    {

        // Force an error
        //$a = null;
        //$a->metodo();

        $data = $this->getPayload();

        $this->logOutput('tryme', "ENTROU");
        $dataJson = json_decode($data);

        $this->logOutput('tryme', $dataJson);

        sleep(5);

        $this->logOutput('tryme', "SAIU: " . $data . "\n");

        return true;
    }
}
