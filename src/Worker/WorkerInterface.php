<?php

namespace BeansqueueWorker\Worker;

interface WorkerInterface
{
    public function execute();
}
