<?php

namespace BeansqueueWorker\Worker;

use Exception;
use Pheanstalk\Job;
use Pheanstalk\Pheanstalk;
use Symfony\Component\Process\Process;

abstract class BaseWorker implements WorkerInterface, RunningInterface
{

    /**
     * @var \Pheanstalk\PheanstalkInterface
     */
    private $pheanstalk = null;

    /**
     * @var \stdClass
     */
    private $queueConfig = null;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger = null;

    const LIMIT_EXEC_GARBAGE = 3;
    const GLOBAL_TIMEOUT = 600;

    /**
     * Importante:
     *    Se tivermos várias instancias do Pheanstalk, por algum problema,
     *    ele não consegue localizar o JOB para fazer um 'delete' ou 'bury'
     *    dando o erro 'NOT_FOUND' no beanstalk.
     *    Ao que parece o JOB fica vinculado ao cliente (Pheanstalk) que o
     *    reservou. Assim temos que ter esse objeto.
     *
     * @return \Pheanstalk\Pheanstalk
     * @throws \Exception
     */
    protected function getPheanstalk()
    {
        if ($this->pheanstalk === null) {
            if (empty($this->queueConfig->serverIp)) {
                throw new \Exception('You need to set the serverIp property');
            }
            $this->pheanstalk = new Pheanstalk($this->queueConfig->serverIp);
        }

        return $this->pheanstalk;
    }

    /**
     * @param $queueName
     * @param $logger \Psr\Log\LoggerInterface
     * @param string $queueConfig
     * @return \BeansqueueWorker\Worker\BaseWorker
     * @throws \Exception
     */
    public static function factoryWorker($queueName, $logger, $queueConfig)
    {
        $queueName = str_replace('.', '\\', $queueName);

        $instance = new $queueName();

        if (!($instance instanceof BaseWorker)) {
            throw new \Exception(
                "The queue '$queueName' need be an instance of BaseWorker"
            );
        }

        if (!empty($queueConfig) && !($queueConfig instanceof \stdClass)) {
            throw new \Exception('queueConfig must be an \\stdClass');
        }

        $instance->logger = $logger;
        $instance->queueConfig = $queueConfig;

        return $instance;
    }

    // <editor-fold defaultstate="collapsed" desc=" Worker Interface ">

    public function execute()
    {
        throw new Exception('Você precisa implementar');
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" Internal Functions ">
    /**
     *
     */
    protected function registerRunningWorker()
    {
        return true;
    }

    /**
     * Registra os handlers de execução dos processo.
     *
     * @var JobModel[]
     */
    protected $threadList = [];

    /**
     * Percorre os processos que estão sendo executados e verifica se eles já terminaram
     * e em qual status.
     *
     * Executa os procedimentos apropriados para 'DELETE' ou 'BURY' os JOBS do Beanstalk.
     */
    protected function threadGarbage()
    {
        $clearList = [];

        // Check for the status of the Process Execution
        foreach ($this->threadList as $key => $object) {
            // Verifica se está terminado
            if ($object->getHandler()->isTerminated()) {
                //
                $this->logOutput($object->getJob()->getId(), $object->getHandler()->getOutput());

                if ($object->getHandler()->isSuccessful()) {
                    $this->getPheanstalk()->delete($object->getJob());
                } else {
                    $this->getPheanstalk()->bury($object->getJob());
                }
                $clearList[] = $key;

                // Verifica se deu timeout
            } elseif (time() >= $object->getTimeoutDate()) {
                $this->logOutput($object->getJob()->getId(), $object->getHandler()->getOutput());
                $this->logOutput($object->getJob()->getId(), '---- ENCERRADO POR TIMEOUT ----');
                $this->logOutput($object->getJob()->getId(), 'Ended at ' . date("Y-m-d H:i:s"));
                $this->getPheanstalk()->bury($object->getJob());
                $object->getHandler()->stop(1, SIGKILL);
                $clearList[] = $key;

                // Verifica se ainda não começou para dar o start
            } elseif (!$object->getHandler()->isStarted()) {
                $object->getHandler()->setTimeout(BaseWorker::GLOBAL_TIMEOUT);
                $object->getHandler()->start();
            }
        }

        // Remove from the threadList that was terminated.
        foreach ($clearList as $value) {
            unset($this->threadList[$value]);
        }
    }

    protected function logOutput($name, $output)
    {
        if (!is_string($output)) {
            $output = print_r($output, true);
        }
        $this->logger->debug(sprintf("[%s]: %s", $name, $output));
    }

    /**
     * Cria a Thread do JOB e adiciona ao ThreadList.
     *
     * @param Job $job
     * @param string $cmd
     * @param int $timeout
     */
    protected function threadEnqueue(Job $job, $cmd, $timeout)
    {
        // Checa se excedeu o limite de threads e ficar em Waiting
        while (count($this->threadList) >= $this->queueConfig->maxThreads) {
            sleep(1);
            $this->threadGarbage();
        }

        $this->logger->debug('processing job ' . $job->getId());

        // De acordo com issue https://github.com/symfony/symfony/issues/5030
        $process = new Process("exec $cmd");
        $obj = new JobModel($process, $job, $timeout);
        $this->threadList[] = $obj;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" Running Interface ">

    /**
     * @param string $queueName
     */
    public function pop($queueName)
    {
        $queueName = ltrim(str_replace('\\', '.', $queueName), '\\');

        $this->logger->debug('Starting listening: ' . $queueName);

        $this->getPheanstalk()->useTube($queueName);

        while (true) {
            if ($this->getPheanstalk()->getConnection()->isServiceListening()) {
                $this->watchQueue($queueName);
            } else {
                $this->logger->critical("Cant get job.\n");
            }

            $this->threadGarbage();
        }
    }

    /**
     * @param string $queueName
     * @throws \Exception
     */
    protected function watchQueue($queueName)
    {
        if (empty($this->queueConfig->autoloadPath) || empty($this->queueConfig->workingPath)) {
            throw new \Exception('queueConfig::autoloadPath and queueConfig::workingPath must be valid');
        }

        $count = 0;

        // Watch the queue
        while ($job = $this->getJob($queueName)) {
            $this->registerRunningWorker();
            $this->logger->debug("caught job ".$job->getId()."\n");

            $dataDecode = json_decode($job->getData());

            $dataEncode = urlencode(json_encode($dataDecode->data));
            $timeout = $dataDecode->timeout;

            $command = "daemonize run '\\BeansqueueWorker\\Process\\CronTab::execute' "
                . "'{$this->queueConfig->autoloadPath}' "
                . "'{$this->queueConfig->workingPath}' "
                . "'queueName={$queueName}' "
                . "'payload={$dataEncode}'";

            $this->threadEnqueue(new Job($job->getId(), $job->getData()), $command, $timeout);

            $count = (($count+1) % self::LIMIT_EXEC_GARBAGE);
            if ($count == 0) {
                $this->threadGarbage();
            }
        }
    }

    public function getJob($class)
    {
        return $this->getPheanstalk()
            ->watch($class)
            ->ignore('default')
            ->reserve(1);
    }

    public function getPayload()
    {
        return urldecode(isset($_REQUEST['payload']) ? $_REQUEST['payload'] : null);
    }
    // </editor-fold>
}
