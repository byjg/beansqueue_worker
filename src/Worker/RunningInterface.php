<?php

namespace BeansqueueWorker\Worker;

interface RunningInterface
{
    public function pop($queueName);
    public function getPayload();
}
