<?php

namespace BeansqueueWorker\Worker;

use Pheanstalk\Job;
use Symfony\Component\Process\Process;

class JobModel
{
    protected $handler;
    protected $job;
    protected $timeoutDate;

    public function __construct(Process $handler, Job $job, $timeout)
    {
        $this->handler = $handler;
        $this->job = $job;
        $this->timeoutDate = time() + $timeout;
    }

    /**
     *
     * @return Process
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     *
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     *
     * @return int
     */
    public function getTimeoutDate()
    {
        return $this->timeoutDate;
    }
}
