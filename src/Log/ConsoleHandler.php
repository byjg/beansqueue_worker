<?php
/**
 * User: jg
 * Date: 14/06/17
 * Time: 11:09
 */

namespace BeansqueueWorker\Log;

use Monolog\Handler\AbstractProcessingHandler;

class ConsoleHandler extends AbstractProcessingHandler
{
    protected function write(array $record)
    {
        echo $record['formatted'];
    }
}