<?php

namespace BeansqueueWorker\Process;

use BeansqueueWorker\Log\ConsoleHandler;
use BeansqueueWorker\Worker\BaseWorker;
use Exception;
use Monolog\Logger;

class CronTab
{
    protected $loop = 0;

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger()
    {
        $logger = new Logger('Beansqueue');
        $logger->pushHandler(new ConsoleHandler());
        return $logger;
    }

    public function execute()
    {
        $started = new \DateTime();

        if (empty($_REQUEST['queueName'])) {
            throw new Exception('Você deve passar o parametro com o nome da Queue "queue=QUEUE"');
        }
        if (empty($_REQUEST['payload'])) {
            throw new Exception('Você deve passar o parametro com o payload "payload={}"');
        }
        $queueName = $_REQUEST['queueName'];

        $logger = $this->getLogger();
        $logger->info("Executing '$queueName'... \n\n");

        $thread = BaseWorker::factoryWorker($queueName, $logger, new \stdClass());

        $exitCode = 0;
        try {
            if ($thread->execute()) {
                $logger->info("Execução OK!");
            } else {
                $logger->info("Execução Falhou!");
                $exitCode = 1;
            }
        } catch (\Exception $ex) {
            $logger->error($ex);
            $exitCode = 2;
        }

        $interval = (new \DateTime())->diff($started);
        $logger->info("Elapsed: " . $interval->format('%d days %H:%I:%S'));

        exit($exitCode);
    }
}
