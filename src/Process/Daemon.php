<?php

namespace BeansqueueWorker\Process;

use BeansqueueWorker\Log\ConsoleHandler;
use BeansqueueWorker\Worker\BaseWorker;
use Exception;
use Monolog\Logger;

class Daemon
{
    protected $loop = 0;

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger()
    {
        $logger = new Logger('Beansqueue');
        $logger->pushHandler(new ConsoleHandler());
        return $logger;
    }

    public function execute()
    {
        if (empty($_REQUEST['queueName'])) {
            throw new Exception('Você deve passar o parametro com o nome da Queue "queueName=QUEUE"');
        }
        $queueName = $_REQUEST['queueName'];

        $queueConfig = null;
        if (!empty($_REQUEST['queueConfig'])) {
            $queueConfig = $_REQUEST['queueConfig'];
        }

        $logger = $this->getLogger();
        $logger->info("Loading Daemon... \n\n");

        $queueConfig = json_decode($queueConfig);

        // Warning: This is very coupled with Daemonize and only will work with
        // DAEMONIZE RUN.
        // DAEMONIZE INSTALL MUST SET THIS VALUES
        $daemonizeArguments = $GLOBALS['argv'];
        if (empty($queueConfig->autoloadPath)) {
            $queueConfig->autoloadPath = $daemonizeArguments[3];
        }
        if (empty($queueConfig->workingPath)) {
            $queueConfig->workingPath = $daemonizeArguments[4];
        }
        // End warning

        $logger->info("Id: $queueName, Queue Config: " . json_encode($queueConfig));
        register_shutdown_function([$this, 'shutdown'], $logger);
        // pcntl_signal(SIGINT, [$this, 'sigint']);
        // pcntl_signal(SIGTERM, [$this, 'sigint']);

        $thread = BaseWorker::factoryWorker($queueName, $logger, $queueConfig);
        $thread->pop($queueName);

        return false; // Se chegou até aqui não precisa continuar.
    }

    /**
     * @param $logger \Psr\Log\LoggerInterface
     */
    public function shutdown($logger = null)
    {
        $logger->warning('Worker Shutdown');
    }

    // public function sigint()
    // {
    //     exit;
    // }
}
